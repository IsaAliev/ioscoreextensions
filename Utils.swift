import Foundation
import UIKit

public class Utils {
    static let executableName = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String
    static var iPadSourceView: UIView?
    
    public static func swiftClassFromString(_ className: String) -> AnyClass? {
        return NSClassFromString(executableName + "." + className)
    }
    
    public static func localized(_ string: String) -> String {
        return NSLocalizedString(string, comment: "")
    }

    public static func stringFromSwiftClass(_ swiftClass: Any) -> String {
        return NSStringFromClass(swiftClass as! AnyClass).components(separatedBy: ".").last!
    }
    
    public static func screenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    public static func appVersion() -> String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
}

extension UIResponder {
    @objc func getController() -> UIViewController? {
        next?.getController()
    }
}

public func weakify<T: AnyObject>(_ target: T, _ f: @escaping (T) -> () -> Void) -> (() -> Void) {
    return { [weak target] in
        guard let target = target else { return }
        
        f(target)()
    }
}

public func weakify<T: AnyObject, A>(_ target: T, _ f: @escaping (T) -> (A) -> Void) -> ((A) -> Void) {
    return { [weak target] a in
        guard let target = target else { return }
        
        f(target)(a)
    }
}

public func localizePlural(key: String, count: Int, languageCode: String) -> String? {
    let language = languageCode
    guard let path = Bundle.main.path(forResource: language, ofType: "lproj"),
        let bundle = Bundle(path: path) else {
            return nil
    }
    let formatString = NSLocalizedString(key, tableName: nil, bundle: bundle, value: "", comment: "")
    let resultString = String(format: formatString, locale: Locale.init(identifier: language), count)
    return resultString
}
