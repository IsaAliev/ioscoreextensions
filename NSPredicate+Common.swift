//
//  NSPredicate+Common.swift
//  Calendar
//
//  Created by Isa Aliev on 08.09.2020.
//  Copyright © 2020 IA. All rights reserved.
//

import Foundation

public extension NSPredicate {
    static var validEmail: NSPredicate {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        return NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    }
}
