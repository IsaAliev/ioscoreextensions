//
//  Date+Utils.swift
//  Water
//
//  Created by Aliev Isa on 27/01/2020.
//  Copyright © 2020 Aliev Isa. All rights reserved.
//

import Foundation

public extension Date {
    func hoursAndMinutes() -> (hours: Int, minutes: Int) {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute], from: self)
        
        return (components.hour ?? 0, components.minute ?? 0)
    }
    
    static func from(hours: Int, andMinutes minutes: Int) -> Date {
        let calendar = Calendar.current
        let components = DateComponents(calendar: calendar, hour: hours, minute: minutes)
        
        return calendar.date(from: components) ?? Date()
    }
}

public extension Date {
    var isToday: Bool {
        let todayComponents = Calendar.current.dateComponents([.year, .month, .day], from: Date())
        let selfComponents = Calendar.current.dateComponents([.year, .month, .day], from: self)
        
        return todayComponents.day! == selfComponents.day! && todayComponents.month! == selfComponents.month! && todayComponents.year! == selfComponents.year!
    }
    
    func plusDays(_ days: Int) -> Date {
        let calendar = Calendar.current
        let date = calendar.startOfDay(for: self)

        return calendar.date(byAdding: .day, value: days, to: date)!
    }
    
    func daysFrom(_ date: Date) -> Int {
        let calendar = Calendar.current
        
        return calendar.dateComponents([.day], from: date, to: self).day!
    }
    
    func daysTo(_ date: Date) -> Int {
        let calendar = Calendar.current
        
        return calendar.dateComponents([.day], from: self, to: date).day!
    }
    
    func agoMultipliedBy(_ mult: TimeInterval) -> Date {
        return Date(timeIntervalSinceNow: self.timeIntervalSinceNow * 2)
    }

    func forwardWith(_ time: TimeInterval) -> Date {
        return Date(timeInterval: time, since: self)
    }
    
    static func today(plusDays: Int = 0) -> Date {
        let calendar = Calendar.current
        let currentDateComponents = calendar.dateComponents([.day, .month, .year], from: Date())
        let today = calendar.date(from: currentDateComponents) ?? Date()

        return calendar.date(byAdding: .day, value: plusDays, to: today) ?? today
    }
    
    static func thisWeek(plusWeeks: Int = 0) -> Date {
        var calendar = Calendar.current
        calendar.firstWeekday = 2
        let currentDateComponents = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date())
        let thisWeek = calendar.date(from: currentDateComponents) ?? Date()
        
        return calendar.date(byAdding: .day, value: plusWeeks * 7, to: thisWeek) ?? thisWeek
    }
    
    static func thisMonth(plusMonths: Int = 0) -> Date {
        var currentDateComponents = Calendar.current.dateComponents([.month, .year], from: Date())
        currentDateComponents.day = 1
        let thisMonth = Calendar.current.date(from: currentDateComponents) ?? Date()
        
        return Calendar.current.date(byAdding: .month, value: plusMonths, to: thisMonth) ?? thisMonth
    }
}

public extension Date {
    var yyyymmdd: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        formatter.locale = Locale(identifier: "en")
        
        return formatter.string(from: self)
    }
    
    func datesByDayTo(_ date: Date) -> [Date] {
        var res = [Date]()
        let daysFromSelf = date.daysFrom(self)
        
        guard daysFromSelf >= 0 else {
            return []
        }
        
        for i in 0...daysFromSelf {
            res += [plusDays(i)]
        }
        
        return res
    }
    
    var year: Int {
        Calendar.current.component(.year, from: self)
    }
    
    var month: Int {
        Calendar.current.component(.month, from: self)
    }
    
    var day: Int {
        Calendar.current.component(.day, from: self)
    }
    
    var millis: Int {
        Int(timeIntervalSince1970 * 1000)
    }
}
