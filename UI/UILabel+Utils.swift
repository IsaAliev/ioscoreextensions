//
//  UILabel+Utils.swift
//  Calendar
//
//  Created by Isa Aliev on 08.06.2020.
//  Copyright © 2020 IA. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel {
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"color:\(textColor.toHexString());font-family: '-apple-system', '\(font.familyName)'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)

        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
    
    func with(_ font: UIFont, color: UIColor) -> UILabel {
        self.font = font
        self.textColor = color
        
        return self
    }
    
    func with(_ text: String) -> UILabel {
        self.text = text
        
        return self
    }
}

public extension UILabel {
    func applyGradientWith(startColor: UIColor, endColor: UIColor) -> Bool {

        var startColorRed: CGFloat = 0
        var startColorGreen: CGFloat = 0
        var startColorBlue: CGFloat = 0
        var startAlpha: CGFloat = 0

        if !startColor.getRed(&startColorRed, green: &startColorGreen, blue: &startColorBlue, alpha: &startAlpha) {
            return false
        }

        var endColorRed: CGFloat = 0
        var endColorGreen: CGFloat = 0
        var endColorBlue: CGFloat = 0
        var endAlpha: CGFloat = 0

        if !endColor.getRed(&endColorRed, green: &endColorGreen, blue: &endColorBlue, alpha: &endAlpha) {
            return false
        }

        let gradientText = self.text ?? ""
        
        let textSize: CGSize = gradientText.size(withAttributes: [.font: self.font as Any])
        let width: CGFloat = textSize.width
        let height: CGFloat = textSize.height

        UIGraphicsBeginImageContext(CGSize(width: width, height: height))

        guard let context = UIGraphicsGetCurrentContext() else {
            UIGraphicsEndImageContext()
            return false
        }

        UIGraphicsPushContext(context)

        let glossGradient: CGGradient?
        let rgbColorspace: CGColorSpace?
        let num_locations: size_t = 2
        let locations: [CGFloat] = [0.0, 1.0]
        let components: [CGFloat] = [startColorRed, startColorGreen,startColorBlue, startAlpha,
                                     endColorRed, endColorGreen, endColorBlue, endAlpha]
        rgbColorspace = CGColorSpaceCreateDeviceRGB()
        glossGradient = CGGradient(colorSpace: rgbColorspace!, colorComponents: components, locations: locations, count: num_locations)
        let topCenter = CGPoint(x: .zero, y: textSize.height / 2.0)
        let bottomCenter = CGPoint(x: textSize.width, y: textSize.height / 2.0)
        context.drawLinearGradient(glossGradient!, start: topCenter, end: bottomCenter, options: CGGradientDrawingOptions.drawsBeforeStartLocation)

        UIGraphicsPopContext()

        guard let gradientImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return false
        }

        UIGraphicsEndImageContext()

        self.textColor = UIColor(patternImage: gradientImage)

        return true
    }

}
