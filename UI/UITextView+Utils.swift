//
//  UITextView+Utils.swift
//  Calendar
//
//  Created by Isa Aliev on 14.04.2020.
//  Copyright © 2020 IA. All rights reserved.
//

import Foundation
import UIKit

public extension UITextView {
    var textHeight: CGFloat {
        let width = frame.width - textContainerInset.left - textContainerInset.right - 4.0
        
        return text.height(width, attributes: [.font: font as Any])
    }
}
