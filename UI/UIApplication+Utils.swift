//
//  UIApplication+Utils.swift
//  uVPN
//
//  Created by Isa Aliev on 23.08.2022.
//

import UIKit

public extension UIApplication {
    static func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
    }
    
    static var safeAreaInsets: UIEdgeInsets {
        if #available(iOS 15.0, *) {
            return (shared.connectedScenes
                .first(where: { $0.activationState == .foregroundActive || $0.activationState == .foregroundInactive }) as? UIWindowScene)?
                .keyWindow?.safeAreaInsets ?? .zero
        } else {
            return shared.keyWindow?.safeAreaInsets ?? .zero
        }
    }
}

