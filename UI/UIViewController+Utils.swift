import UIKit

public extension UIViewController {
    @objc class var loadFromXib: Bool {
        return false
    }

    class func controller() -> Self {

        func controller<T>(_ type: T.Type) -> T {
            if (loadFromXib) {
                return fromXib() as! T
            } else {
                let storyboard = UIStoryboard(name: storyboardName(), bundle: nil)
                return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier()) as! T
            }
        }

        return controller(self)
    }

    @objc class func storyboardName() -> String {
        return "Main"
    }
    
    class func storyboardIdentifier() -> String {
        return Utils.stringFromSwiftClass(self)
    }
    
    func cleanBackButton() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

public extension UIViewController {
    func makeNonDismissable() {
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
}

public extension UIImage {
    class func imageWithLabel(label: UILabel) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

fileprivate struct AssociatedKey {
    static var kDeinitObserver = "kDeinitObserver"
}

public extension UIViewController {
    var deinitHandler: (() -> ())? {
        set {
            objc_setAssociatedObject(self, &AssociatedKey.kDeinitObserver, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
        
        get {
            objc_getAssociatedObject(self, &AssociatedKey.kDeinitObserver) as? () -> ()
        }
    }
    
    func onDeinit(_ handler: @escaping () -> ()) {
        deinitHandler = handler
    }
}
