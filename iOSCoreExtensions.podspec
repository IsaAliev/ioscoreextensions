Pod::Spec.new do |spec|
  spec.name         = "iOSCoreExtensions"
  spec.version      = "0.0.1"
  spec.summary      = "Extensions"
  spec.description  = <<-DESC
My iOS Core Extesnions
                   DESC

  spec.homepage     = "https://github.com/IsaAliev/"

  # spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author       = { "IsaAliev" => "isaaliev12@gmail.com" }

  spec.platform     = :ios, "11.2"

  spec.source       = { :path => "/Users/isaaliev/myLibs/iOSCoreExtensions" }

  spec.source_files  = "*.{swift}"

  spec.swift_version = "5.0" 
end
