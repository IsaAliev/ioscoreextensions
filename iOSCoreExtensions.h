//
//  iOSCoreExtensions.h
//  iOSCoreExtensions
//
//  Created by Isa Aliev on 18.09.2020.
//  Copyright © 2020 IA. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for iOSCoreExtensions.
FOUNDATION_EXPORT double iOSCoreExtensionsVersionNumber;

//! Project version string for iOSCoreExtensions.
FOUNDATION_EXPORT const unsigned char iOSCoreExtensionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iOSCoreExtensions/PublicHeader.h>


