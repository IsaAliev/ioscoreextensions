// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "iOSCoreExtensions",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "iOSCoreExtensions",
            targets: ["iOSCoreExtensions"]
		),
    ],
    targets: [
        .target(
            name: "iOSCoreExtensions",
			dependencies: [],
			path: "",
			exclude: ["README.md", "Info.plist", "iOSCoreExtensions.podspec"]
		)
    ]
)
